# Red Black Tree

## How to Compile and Run Red Black Tree
Take the java file called redBlackTree.java and open it up in the IDE of your
choice. We used eclipse. Hit run and our test output will be printed to the console.

## How to Compile and Run Hash Map
~~~~ READ ME FILE ~~~~
~~~~ HashMapping Emergency Department Information Systems application ~~~~

1. Run mainMenu.java


2. System Main Menu will Display :


	*********    Main Menu     *************
	Press "1" for adding entries to the HashMap:  
	Press "2" for displaying all entries from the Database:  
	Press "3" for searching records in the database:  
	Press "4" for deleting entries to the HashMap:  
	Press "5" for exiting from the Main menu Your Response: 


3. Inserting, Searching and Deleting functions all accessible using number inputs


4. Pressing "1" will take user into a new screen, and then will prompt user to enter name of a patient. 
   This is a LOOP and will keep prompting user to enter name until user enters "exit" (case insensitive).
   Error validation is present in insert method. Name entered must be [FIRST NAME] and [LAST NAME].


	Entering in method 1
	enter "Exit" to return to main menu or 
	Please enter the patient's name to add to system:  

	vishal teji (User Entered)
	vishal teji has been added to the system.


5. Pressing "2" will display all patients and respective keys that have been logged into the hashmap EDIS.
   When this display method is called, it iterates through the hashmap, printing all the values and then it exits into the main menu


	***2. Entering a portal for displaying all the entries in the Database.***
	vt1 has got patient named: vishal teji
	sc2 has got patient named: simrat chaniana
	kg3 has got patient named: kamaljit grewal
	js6 has got patient named: james smith
	mu4 has got patient named: manvir uppal
	sl5 has got patient named: sunil lotay

	Exiting portal and returning to Main Menu 


6. Pressing "3" will prompt the user to SEARCH the hash mapped data.
   Search method allows user to search via NAME or KEY
   Error validation is present in Search method. If no information is found, system will respond respectively.
   

	***3. Entering in portal to Search entry.***
	Press 1 to search with key
	Press 2 to search with Name
	Press 3 to go to Main menu: Your Response: 1
	Please enter the KEY search word:

	vt5 (USER ENTERED ERROR)

	Sorry our system indicate that no such record found matching your keywords "vt5" in
	our Database.

	Press 1 to search with key
	Press 2 to search with Name
	Press 3 to go to Main menu: Your Response: 1
	Please enter the KEY search word:

	vt1 (USER ENTERED CORRECT)

	Match found
	Our records shows that : vt1 has got patient named: vishal teji


7. Pressing "4" will prompt user to search for an entry to DELETE from the hashmapped data.
   As mentioned in report, this method is built on top of the search method.
   When looking to delete a certain value, you are able to look it up via NAME or KEY.
   Once you have found the value you want to delete, the program will do it for you.
   Error validation is present in DELETE method. If no information is found, system will respond respectively.


	***4.Entering in portal to Delete entry.***
	Press 1 to delete Record using key
	Press 2 to delete Record using Name
	Press 3 to go to Main menu:
	Your Response: 1
	Please enter the KEY search word:

	sl5

	Record found
	sunil lotay associated with the key entered sl5 has been deleted from our system


8. Lastly pressing "5" will shut down the system.


