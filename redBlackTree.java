public class redBlackTree {
	/*
	 * class redBlackTree implements a Red Black Tree, a special type of binary search tree.
	 * @InstanceVar root stores the root of the red black tree.
	 * @InstanceVars RED and BLACK store one bit of information on whether a node is red or black,
	 * 					If a red/black pattern or rules are broken we know we need to restructure the tree.
	 * Includes class Node, and functions: searchNode, insertNode, repairTreePostInsert, deleteNode, fixColourGivenBlack, and deleteNodeZero.
	 * 					It has helper functions: replaceParent, findMin, rightRotation, and rotateLeft.
	 * 
	 * @authors Manvir Uppal and Kamaljit Grewal.
	 *
	 */

	protected Node root;

	boolean RED = false;
	boolean BLACK = true;

	public static void main(String[] args) {
		
		/*
		 * Test Case 1
		 * Insert an element, the search for it. Print it
		 * This shows that insertNode and seachNode work.
		 */	
		redBlackTree tree = new redBlackTree();
		tree.insertNode("key1");
		Node foundNode = tree.searchNode("key1");
		System.out.println("We have found: " + foundNode.element);
		
		/*
		 * Test Case 2
		 * Insert an element, the search for it. Print it.
		 * Then delete the node, search for it again, and print it.
		 * Should print the element the first time, then print null the second time.
		 * Shows that delete works.
		 */
		redBlackTree tree2 = new redBlackTree();
		tree2.insertNode("apples");
		Node found = tree2.searchNode("apples");
		System.out.println("We have found: " + found.element);
		tree2.deleteNode("apples");
		found = tree2.searchNode("apples");
		System.out.println("We have found: " + found);
		
		
		/*
		 * Test case 3
		 * search for element not that was never there.
		 * Should print null.
		 */
		found = tree2.searchNode("tangerines");
		try {
			System.out.println("We have found: " + found.element);
		}catch (NullPointerException e){
			System.out.println("We have found: " + found);
		}
		
		/*
		 * Test case 4
		 * A large tree with lots of elements
		 */
		
	} //end of main

	public class Node {
		/*
		 * class nodes defines Node objects.
		 * @InstanceVar element is a String. Holds the data inside a Node.
		 * @InstanceVar colour holds a boolean true is the Node's colour is BLACK, or false if it's RED.
		 * @InstanceVar parent is a Node object. Holds new Node's parent Node or null.
		 * @InstanceVar left is a Node object. Holds new Node's left child Node or null.
		 * @InstanceVar right is a Node object. Holds new Node's right child Node or null.
		 */
		
		String element;
		boolean colour;
		
		Node parent;
		Node left;
		Node right;
		
		public Node (String element) {
			/*
			 * This constructor Node(String element) creates a Node object. 
			 */
			this.element = element;
		} //end of Node constructor
	} //end of public class Node

	private class NilNode extends Node {
		  private NilNode() {
		    super(null, 0, null, null);
		    this.colour = false;
		  }
	}

	public Node searchNode(String key) {
		
		/*
		 * Method goes down the tree until we find the node or return null
		 * Throws null if node not found
		 * 
		 */
		
	    Node node = root;
	    while (node != null) { //Go down tree until at the bottom 
	      if (key.equals(node.key)) { //If found do following
	        return node;
	      } else if (key.compareTo(node.key) < 0) {// To determine if in the right or left child 
	        node = node.left; //Make child node new node to search
	      } else {
	        node = node.right; //Make child node new node to search
	      }
	    }

	    return null;//If not found 
	  }

    return null;
	} //end of searchNode function


	public void insertNode (String newElement) {
		/*
		 * Method insertNode adds a new Node to the tree. Goes down the tree until we find the right spot.
		 * Throws IllegalArgumentException if newElement is already in the tree.
		 * Otherwise, Calls the Node(String element) constructor to create the Node. Makes it a RED Node.
		 * Call repairTreePostInsert() to maintain the red-black pattern.
		 * 
		 */
		Node currentNode = root;
		Node parent = null;
		
		while (currentNode != null) {  //move through the tree to find the right spot to insert it
			parent = currentNode;
			
			//remember, should be: leftChild < parent(currentNode) < rightChild
			if (newElement.compareTo(currentNode.element) < 0) {
				currentNode = currentNode.left;
			}
			else if (currentNode.element.compareTo(newElement) < 0) {
				currentNode = currentNode.right;
			}
			else {
				//if newElement is not new & already in the tree, throw exception, we can't add it
				throw new IllegalArgumentException(newElement + " is already in the tree! Can't add it.");
			}
		} //end of while node != null
		
		Node newNode = new Node(newElement); //create the node
		newNode.colour = RED;
		
		if (parent == null) { //if the node we're trying to add is the first/only node in the tree, it's the root
			root = newNode;
		}
		else if (newElement.compareTo(parent.element) > 0) {  //determine if the newNode is a right or left child
			parent.right = newNode;
		}
		else {
			parent.left = newNode;
		}		
		try {
			newNode.parent = parent;
		} catch (NullPointerException e) {
			newNode.parent = null;
		}
		repairTreePostInsert(newNode); //make sure the red black tree rules and pattern are maintained.
	} //end of insertNode


	private void repairTreePostInsert(Node newNode) {
		/*
		 * This method repairs the tree, and makes sure the red black tree rules and pattern are maintained.
		 * @param Node NewNode is the node that was just inserted,a nd we're going to pay attention to this node, its parent, auntie, & grandparent.
		 * 
		 * There are 5 repair cases depending on whether the newNode is the root, the colour of auntie and parent, 
		 * and whether the inserted node is an outer or inner node.
		 */
		
		Node parent = newNode.parent;
		
		// Case 1: new node is root
		if (newNode == root || newNode.parent == null) {
			newNode.colour = BLACK;
		}
		
		Node grandparent;
		
		try {
			grandparent = parent.parent;
		} catch (NullPointerException e) {
			grandparent = null;
		}
		
		
		//case 2: newNode is always RED (see insertNode method). parent node is red. parent is root. can't have two reds together.
		if (newNode.parent == root && newNode.parent.colour == RED) {
			newNode.parent.colour = BLACK;
		}		
		
		//case 3: parent & auntie nodes both are red
		if (newNode.parent != null && newNode.parent.colour == RED) {  //if parent red
			
			//if parent is a right child of grandparent
			if (grandparent.right == parent) {
				if (grandparent.left.colour == RED) { //if auntie also red
					//make parent and auntie black
					parent.colour = BLACK;
					grandparent.left.colour = BLACK;					
				}
			} //end of if parent is a right child
			
			//if parent is a left child
			else if (grandparent.left == parent) {
				if (grandparent.right.colour == RED) { //if auntie red
					//make parent and auntie black
					parent.colour = BLACK;
					grandparent.right.colour = BLACK;
				
				}
			} //end of else if parent is a left child
			
			//make grandparent red
			grandparent.colour = RED;
			
			//if grandparent and great grandparent are BOTH red, will need to call again!
			if (grandparent.colour == RED && grandparent.parent.colour == RED) {
				repairTreePostInsert(grandparent);
			} //end of grandparent and great-grandparent repair
			
		} //end of case 3
		
		
		//case 4: parent red, auntie black, inserted node is "inner grandchild"
		//case 5: parent red, auntie black, inserted node is "outer grandchild"
		//we've got to move things around so we don't have a child and parent both red.
		//and it's a binary tree, so we want 0 or 2 children per RED node. Perform rotations to fix this.
		else if (grandparent!= null && parent == grandparent.left) {
			Node originalGrandma = grandparent;
			
			if (newNode == parent.right) {
				rotateLeft(parent);
				parent = newNode;
			}
			
			rightRotation(grandparent);
			
			parent.colour = BLACK;
			originalGrandma.colour = RED;
		} //end of else if (parent == grandparent.left)
		
		else if (grandparent != null && parent == grandparent.right) {
			Node originalGrandma = grandparent;
			
			if (parent!= null && newNode == parent.right) {
				rightRotation(parent);
				parent = newNode;
			}
			
			rotateLeft(grandparent);
			
			parent.colour = BLACK;
			originalGrandma.colour = RED;
		} //end of else { //parent == grandparent.right
		
	} //end of repairTreePostInsert method


	public void deleteNode(String key) {
		
		//Start at the root
		Node current = root;
		
		//Search to find appropriate node
		while(current != null && !current.key.equals(key)) {
			
			if(key.compareTo(current.key) < 0) {
				// key < current key
				current = current.left;
				
			}else {
				// current key > key
				current = current.right;
			}
			
		}
		
		//Now take the node you found and proceed to delete
		
		Node up;
		boolean colourHold;
		
		//in the case there is one or zero child 
		
		if(current.left == null || current.right == null) {
			colourHold = current.colour;
			//delete appropriate node and fix tree accordingly 
			up = deleteNodeZero(current);
		}
		
		//in the case there is 2 children
		else {
			
			// Find minimum node of right subtree ("inorder " of current node)
		      Node inOrder = findMin(current.right);

		      // Copy inorder 's data to current node 
		      current.key = inOrder.key;

		      // Delete inorder  just as we would delete a node with 0 or 1 child
		      up = deleteNodeZero(inOrder);
		      colourHold = inOrder.colour;
			
		}
		//Now re arrange the colours so it follows the red black rules
		
		 if (colourHold == false) {//if black 
			  fixColourGivenBlack(up);

		      // Remove the temporary NIL node
			  
		      if (up.getClass() == NilNode.class) {
		        replaceParentsChild(up.parent, up, null);
		      }
		      
			  
			  
		    }
		
	}
	
	public void fixColourGivenBlack(Node node) {
		Node parent = node.parent;

		  // Parent is null colours are in appropriate order end of the recursion
		  if (parent == null) {
		    
		    return;
		  }

		  // Parent is black --> nothing to do
		  if (parent.colour == false) {
		    return;
		  }

		  // parent is red
		  Node grandparent = parent.parent;

		  
		  // if the parent is the root
		  if (grandparent == null) {
		    // In that case Recolour the root black.
		    parent.colour = false;
		    return;
		  }

		  // Get the uncle 
		  Node uncle = getUncle(parent);

		  // Uncle is red we then recolour parent, grandparent and uncle
		  if (uncle != null && uncle.colour == true) {
		    parent.colour = false;
		    grandparent.colour = true;
		    uncle.colour = false;

		    // Call recursively to fix colour
		    fixColourGivenBlack(grandparent);
		  }

		  // Parent is left child of grandparent
		  else if (parent == grandparent.left) {
		    if (node == parent.right) {
		      rotateLeft(parent);
		      parent = node;
		    }

		    // Uncle is black and node is left->left of its grandparent
		    rightRotation(grandparent);

		    // Recolour original parent and grandparent
		    parent.colour = false;
		    grandparent.colour = true;
		  }

		  // Parent is right child of grandparent
		  else {
		    //Uncle is black and node is right->left of its grandparent
		    if (node == parent.left) {
		    	rightRotation(parent);

		      // Let "parent" point to the new root node of the rotated sub-tree.
		      parent = node;
		    }

		    // Uncle is black and node is right->right of its grandparent
		    rotateLeft(grandparent);

		    // Recolour original parent and grandparent
		    parent.colour = false;
		    grandparent.colour = true;
		  }
	}
	private Node getUncle(Node parent) {
		// TODO Auto-generated method stub
		Node grandparent = parent.parent;
		  if (grandparent.left == parent) {
		    return grandparent.right;
		  } else if (grandparent.right == parent) {
		    return grandparent.left;
		  } else {
			//doesnt exist
		    return null;
		  }
	}
	
	public Node deleteNodeZero(Node node) {
		//delete node with zero or one child
		
		if(node.right == null) {
			
			if(node.left == null) {
				//no child
				
				//temp null
				Node child = null;
				replaceParent(node, node.parent, child);
			}
			else {
				//only left
				replaceParent(node, node.left, node.parent);
				return node.left;
				
			}
			
		}else if(node.left == null) {
			//one right
			replaceParent(node, node.right, node.parent);
			return node.right;
		}
		
		//temp
		return node;
		
	}
	public void replaceParent(Node newC, Node old, Node parent) {
		// TODO Auto-generated method stub
		
		if (parent == null) {
		    root = newC;
		  } else if (parent.left == old) {
		    parent.left = newC;
		  } else if (parent.right == old) {
		    parent.right = newC;
		  } 
		  if (newC != null) {
		    newC.parent = parent;
		  }
		
	}
	private void replaceParentsChild(create.Node parent, create.Node up, Object object) {
		Node temp = null;
		replaceParent(parent,up, temp);
		
	}

	public Node findMin(Node node) {
		
		//return min node by going to bottom left
		
		while (node.left != null) {
		    node = node.left;
		  }
		return node;

	}


	private void rightRotation (Node a) {
		/*
		 * Perform trinode restructuring, to the right
		 * Node a's left child 'b' becomes new root
		 * b's right child is now a's left child
		 */	
		Node aParent = a.parent;
		Node b = a.left;
		a.left = b.right;
		
		if (b.right != null) {
			b.right.parent = a;
		}
		
		b.right = a;
		a.parent = b;
		b.parent = aParent;
		
		if (aParent != null) {
			if (aParent.left == a) {
				aParent.left = b;
			}
			else {
				aParent.right = b;
			}
		} //end of if (aParent != null)	
	} //end of rightRotation

	
	public void rotateLeft(Node node) {
		
		//rotate left
		
		Node parent = node.parent;
		Node right = node.right;
		
		node.right = right.left;
		if(right.left != null) {
			right.left.parent = node;
			
		}
		right.left = node;
		node.parent = right;
		
	}
} //end of class redBlackTree
